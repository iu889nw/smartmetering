package com.smartmetering.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.smartmetering.SmartmeteringApplication;
import com.smartmetering.domain.Power;
import com.smartmetering.domain.PowerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * Created by FS0413 on 23.1.2017.
 */

@Service
public class GetAndSave {

    @Autowired
    private PowerRepository powerRepository;

    @Autowired
    MongoDbFactory mongoFac;

    @Autowired
    MongoTemplate mongoTemp;

    @Autowired
    MongoOperations mongoOperations;

    private final MongoClient mongoClient = new MongoClient("localhost", 27017);
    private final DB db = mongoClient.getDB("test");
    private static final Logger log = LoggerFactory.getLogger(SmartmeteringApplication.class);
    private static final long RUN_TASK_EVERY_15_SEC = 15000L;

    @Scheduled(fixedDelay = RUN_TASK_EVERY_15_SEC)
    public void getAndSave(){
        try {

            RestTemplate restTemplate = new RestTemplate();
            ObjectMapper mapper = new ObjectMapper();

            String powerJson = restTemplate.getForObject("http://bridge2.vse.sk/api/demand/CurrentPower.php?login=elm02&token=736391773918|737f90d8089f1cd6f567274491fa1bd9&serial_number=5544263",String.class);
            Power created =  mapper.readValue(powerJson,Power.class);
            List<Power> data = powerRepository.findAll();

            log.info(data.get(data.size()-1).toString());
            log.info(created.toString());

            if(data.get(data.size()-1).getLastPower().getTimestamp().equals(created.getLastPower().getTimestamp())
                    && data.get(data.size()-1).getLastOperation().getSequence().equals(created.getLastOperation().getSequence())){
                log.info("Power same value");
            }
            else {
                powerRepository.save(created);
                log.info("Power data saved");
            }

        } catch (Exception e){
            log.info(e.toString());
        }
    }

}
