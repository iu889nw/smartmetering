package com.smartmetering.controllers;

import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.smartmetering.domain.Data;
import com.smartmetering.domain.Power;
import com.smartmetering.domain.PowerRepository;
import com.smartmetering.services.GetAndSave;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.*;

@Controller
@RequestMapping("")
public class Consumption {

    @Autowired
    GetAndSave getAndSave;

    @Autowired
    private PowerRepository powerRepository;

    @Autowired
    MongoDbFactory mongoFac;

    @Autowired
    MongoTemplate mongoTemp;

    @Autowired
    MongoOperations mongoOperations;

    private final MongoClient mongoClient = new MongoClient("localhost", 27017);
    private final DB db = mongoClient.getDB("test");

    @RequestMapping(value="/index",method = RequestMethod.GET)
    public String dashboard(Model model) throws Exception {
        List<Power> powerData = powerRepository.findAll(new Sort(Sort.Direction.ASC,"lastPower.timestamp"));

        model.addAttribute("powerData",powerData);
        model.addAttribute("last",powerData.get(powerData.size()-1).getLastPower().getValue());
        model.addAttribute("totalConsump",Power.getTotalConsumption(powerData));

        Date now = new Date();
        Calendar cal = Calendar.getInstance();
        long timestampNow = now.getTime();
        long milisSinceMidnight = (cal.get(Calendar.HOUR_OF_DAY)*60*60*1000) + (cal.get(Calendar.MINUTE)*60*1000) + (cal.get(Calendar.SECOND)*1000) + (cal.get(Calendar.MILLISECOND));
        long startOfDay = (timestampNow - milisSinceMidnight);
        List<Power> todays = powerRepository.findByTimestampBetween(String.valueOf(startOfDay),String.valueOf(timestampNow));
        model.addAttribute("todayConsump",Power.getTodayConsumption(todays));
        return "tabs/dashboard";
    }

    @RequestMapping(value="/timeline",method = RequestMethod.GET)
    public String timeline(Model model) throws Exception {
        List<Power> powerData = powerRepository.findAll(new Sort(Sort.Direction.ASC,"lastPower.timestamp"));
        model.addAttribute("dates",Power.getDates(powerData));
        model.addAttribute("values",Power.getValues(powerData));
        model.addAttribute("size",powerData.size());
        return "tabs/timeline";
    }

    @RequestMapping(value="/timeline15",method = RequestMethod.GET)
    public String timeline15(Model model) throws Exception {

        List<Power> powerData = powerRepository.findAll(new Sort(Sort.Direction.ASC,"lastPower.timestamp"));
        List<Data> data = Power.createHistogramData(powerData);

        try {
            for (int i = 0; i < data.size() - 1; i++) {
                if (data.get(i).value == 0)
                    data.get(i).value = data.get(i-1).value;
            }
        } catch (Exception e){
            System.out.println(e.toString());
        }
        String startDate = Power.getLast7Days(powerData);
        model.addAttribute("data",data);
        model.addAttribute("size",data.size());
        model.addAttribute("startDate",Power.getLast7Days(powerData));

        return "tabs/timeline15";
    }


}
