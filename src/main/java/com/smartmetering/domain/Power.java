package com.smartmetering.domain;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "consumption")
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "timestamp",
        "status",
        "last_power",
        "last_operation",
        "message"
})
public class Power {

    @JsonProperty("timestamp")
    private String timestamp;
    @JsonProperty("status")
    private String status;
    @JsonProperty("last_power")
    private LastPower lastPower;
    @JsonProperty("last_operation")
    private LastOperation lastOperation;
    @JsonProperty("message")
    private String message;

    @JsonProperty("timestamp")
    public String getTimestamp() throws Exception {

        Long aLong = Long.parseLong(this.timestamp);
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Date date = new Date(aLong*1000);
        return timestamp;
    }

    @JsonProperty("timestamp")
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("last_power")
    public LastPower getLastPower() {
        return lastPower;
    }

    @JsonProperty("last_power")
    public void setLastPower(LastPower lastPower) {
        this.lastPower = lastPower;
    }

    @JsonProperty("last_operation")
    public LastOperation getLastOperation() {
        return lastOperation;
    }

    @JsonProperty("last_operation")
    public void setLastOperation(LastOperation lastOperation) {
        this.lastOperation = lastOperation;
    }

    @JsonProperty("message")
    public String getMessage() {
         return message;
    }

    @JsonProperty("message")
    public void setMessage(String message) {
        this.message = message;
    }

    public Power() {
    }

    public Power(String timestamp, String status, LastPower lastPower, LastOperation lastOperation, String message, Map<String, Object> additionalProperties) {
        this.timestamp = timestamp;
        this.status = status;
        this.lastPower = lastPower;
        this.lastOperation = lastOperation;
        this.message = message;
    }
    
    @Override
    public String toString() {
        try {
            return "Power{" +
                    "dateTime=" + lastPower.getTimestamp() +
                    ", lastPower=" + lastPower.getValue() + " W" + '\'' +
                    ", sequency=" + lastOperation.getSequence() + '\'' +
                    '}';
        } catch (Exception e){
            return e.toString();
        }
    }
    /*
            Methods for working with data
     */

    //create data for chart
    public static String[] getDates(List<Power> powerData) throws Exception{

        Integer i = 0;
        String[] chartDates = new String[powerData.size()];
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

        for(Power p: powerData){
            chartDates[i] = p.lastPower.getTimestamp();
            i++;
        }
        return chartDates;
    }
    public static List<Double> getValues(List<Power> powerData) throws Exception{

        Integer i = 0;
        List<Double> chartValues = new ArrayList<Double>();
        for(Power p: powerData){
            chartValues.add(p.lastPower.getValue());
        }
        return chartValues;
    }

    public static List<Data> createHistogramData(List<Power> powerData) throws Exception {

        //intialize
        Integer i = 0;
        Double count;
        Double sum;
        Double average = 0.0;
        Integer middle = Integer.parseInt(powerData.get(i).lastPower.timestamp) + 450;
        Integer upper = Integer.parseInt(powerData.get(i).lastPower.timestamp) + 900;
        List<Data> histData = new ArrayList<>();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Date date;

        while (i < powerData.size()) {
            sum = 0.0;
            count = 0.0;
            //counts sum of values in 15min window
            while (Integer.parseInt(powerData.get(i).lastPower.timestamp) <= upper) {
                sum +=powerData.get(i).lastPower.getValue();
                count++;
                i++;
                if(i > powerData.size()-1)
                    break;
            }
            if(count==0)
                average = 0.0;
            else average = sum / count;
            date = new Date(Long.parseLong(middle.toString())*1000);
            histData.add(new Data(sdf.format(date),average.intValue()));
            upper+=900;
            middle+=900;
        }
        return histData;
    }

    public static String getLast7Days(List<Power> powerData) throws Exception {

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

        Integer last7days =Integer.valueOf(powerData.get(powerData.size()-1).getTimestamp()) - 604800;
        Date date = new Date(Long.parseLong(last7days.toString())*1000);
        return sdf.format(date);
    }

    //create data for gauges
    public static Double getTotalConsumption(List<Power> powerData){

        Double total = 0.0;
        for (Power p: powerData) {
            total+= p.getLastPower().getValue();
        }
        return round((total/powerData.size())/1000,2);
    }
    public static Double getTodayConsumption(List<Power> powerData) throws Exception {

        Double total = 0.0;
        for(Power p: powerData){
            total+=p.getLastPower().getValue();
        }
        return round(total/powerData.size(),2);
    }

    public static double round(double d, int decimalPlace) {
        BigDecimal bd = new BigDecimal(Double.toString(d));
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd.doubleValue();
    }



    //nested object LastPower
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonPropertyOrder({
            "timestamp",
            "value"
    })
    public static class LastPower {

        @JsonProperty("timestamp")
        private String timestamp;
        @JsonProperty("value")
        private Double value;

        @JsonProperty("timestamp")
        public String getTimestamp() throws Exception{
            Long aLong = Long.parseLong(this.timestamp);
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            Date date = new Date(aLong*1000);
            return sdf.format(date);
        }

        @JsonProperty("timestamp")
        public void setTimestamp(String timestamp) {
            this.timestamp = timestamp;
        }

        @JsonProperty("value")
        public Double getValue() {
            return value;
        }

        @JsonProperty("value")
        public void setValue(Double value) {
            this.value = value;
        }

        public LastPower() {
        }

        public LastPower(String timestamp, Double value, Map<String, Object> additionalProperties) {
            this.timestamp = timestamp;
            this.value = value;
        }

        @Override
        public String toString() {
            return "LastPower{" +
                    "timestamp='" + timestamp + '\'' +
                    ", value='" + value + '\'' +
                    '}';
        }
    }

    //nested object LastOperation
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonPropertyOrder({
            "timestamp",
            "sequence",
            "information"
    })
    public static class LastOperation {

        @JsonProperty("timestamp")
        private String timestamp;
        @JsonProperty("sequence")
        private String sequence;
        @JsonProperty("information")
        private String information;
//        @JsonIgnore
//        private Map<String, Object> additionalProperties = new HashMap<String, Object>();

        @JsonProperty("timestamp")
        public String getTimestamp()throws Exception{
            Long aLong = Long.parseLong(timestamp);
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            Date date = new Date(aLong*1000);
            return sdf.format(date);
        }

        @JsonProperty("timestamp")
        public void setTimestamp(String timestamp) {
            this.timestamp = timestamp;
        }

        @JsonProperty("sequence")
        public String getSequence() {
            return sequence;
        }

        @JsonProperty("sequence")
        public void setSequence(String sequence) {
            this.sequence = sequence;
        }

        @JsonProperty("information")
        public String getInformation() {
            return information;
        }

        @JsonProperty("information")
        public void setInformation(String information) {
            this.information = information;
        }

        public LastOperation() {
        }

        public LastOperation(String timestamp, String sequence, String information, Map<String, Object> additionalProperties) {
            this.timestamp = timestamp;
            this.sequence = sequence;
            this.information = information;
        }

        @Override
        public String toString() {
            return "LastOperation{" +
                    "timestamp='" + timestamp + '\'' +
                    ", sequence='" + sequence + '\'' +
                    ", information='" + information + '\'' +
                    '}';
        }
    }
}