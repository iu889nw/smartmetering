package com.smartmetering.domain;

/**
 * Created by FS0413 on 2.3.2017.
 */
public class Data {

    public String date;
    public Integer value;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public Data() {
    }

    public Data(String date, Integer value) {
        this.date = date;
        this.value = value;
    }

    @Override
    public String toString() {
        return "Data{" +
                "date='" + date + '\'' +
                ", value=" + value +
                '}';
    }
}
