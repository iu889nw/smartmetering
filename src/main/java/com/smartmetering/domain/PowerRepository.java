package com.smartmetering.domain;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

/**
 * Created by iviak on 22.1.2017.
 */
public interface PowerRepository extends MongoRepository<Power,Long> {

    List<Power> findByTimestampGreaterThan(String timestamp);
    List<Power> findByTimestampLessThan(String timestamp);
    List<Power> findByTimestampBetween(String from,String to);
    List<Power> findByTimestampLike(String regex);

}
