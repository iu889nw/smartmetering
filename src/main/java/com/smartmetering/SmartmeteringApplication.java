package com.smartmetering;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;

@EnableCaching
@EnableAsync
@EnableScheduling
@SpringBootApplication
public class SmartmeteringApplication {


    public static void main(String[] args) {
        SpringApplication.run(SmartmeteringApplication.class, args);

    }

	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }


}
